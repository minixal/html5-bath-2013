FROM alpine

ENV NAME=html5-bath-2013
ADD ./build /build
ADD ./entrypoint /entrypoint
ADD ./salt /salt

RUN apk --no-cache add bash

RUN bash < /entrypoint

